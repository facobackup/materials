import Vector from "./Vector";

export default class Vector3D extends Vector {
    constructor(x, y, z) {
        super([x, y, z])
    }
}